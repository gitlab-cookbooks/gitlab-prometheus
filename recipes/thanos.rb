require 'yaml'
include_recipe 'gitlab-prometheus::default'

env = node.chef_environment

directory node['thanos']['dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

ark ::File.basename(node['thanos']['dir']) do
  url node['thanos']['binary_url']
  checksum node['thanos']['checksum']
  version node['thanos']['version']
  prefix_root Chef::Config['file_cache_path']
  path ::File.dirname(node['thanos']['dir'])
  owner node['prometheus']['user']
  group node['prometheus']['group']
  action :put
end

# Fetch secrets from gitlab_secrets
unless node['thanos']['gcs-creds']['backend'].nil?
  secrets = node['thanos']['gcs-creds']
  google_json_data = get_secrets(secrets['backend'],
                               secrets['path'],
                               secrets['key'])
  gcs_creds = Base64.decode64(google_json_data['thanos']['gcs-creds']['json_base64'])

  file node['thanos']['gcs-creds']['file'] do
    owner node['prometheus']['user']
    group node['prometheus']['group']
    mode '0600'
    content gcs_creds
  end

  node.default['thanos-sidecar']['env']['GOOGLE_APPLICATION_CREDENTIALS'] = node['thanos']['gcs-creds']['file']
  node.default['thanos-store']['env']['GOOGLE_APPLICATION_CREDENTIALS'] = node['thanos']['gcs-creds']['file']
  node.default['thanos-compact']['env']['GOOGLE_APPLICATION_CREDENTIALS'] = node['thanos']['gcs-creds']['file']
  node.default['thanos-rule']['env']['GOOGLE_APPLICATION_CREDENTIALS'] = node['thanos']['gcs-creds']['file']
end

thanos_store_search = node['thanos']['thanos_store_search']
thanos_store_query = search(:node, thanos_store_search).sort! { |a, b| a[:fqdn] <=> b[:fqdn] }
file node['thanos']['store_inventory'] do
  content YAML.dump(generate_inventory_file(
    thanos_store_query,
    node['thanos']['grpc_port'],
    node['thanos']['thanos_public_hosts'],
    node['thanos']['additional_store_targets']
  ))
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0644'
end

thanos_storage_config = {
  type: node['thanos']['storage']['type'],
  config: node['thanos']['storage']['config'],
}

# Flip flags when Thanos storage is enabled.
if node['thanos']['storage']['enable']
  # Enable the object storage config flag.
  node.default['thanos-sidecar']['flags']['objstore.config-file'] = node['thanos']['storage']['config-file']
  node.default['thanos-store']['flags']['objstore.config-file'] = node['thanos']['storage']['config-file']
  node.default['thanos-compact']['flags']['objstore.config-file'] = node['thanos']['storage']['config-file']
  node.default['thanos-rule']['flags']['objstore.config-file'] = node['thanos']['storage']['config-file']
  # Disable Prometheus compactions, this is handled by Thanos compact component.
  node.override['prometheus']['flags']['storage.tsdb.min-block-duration'] = '2h'
  node.override['prometheus']['flags']['storage.tsdb.max-block-duration'] = '2h'
  # Reduce local retention to 7 days.
  node.default['prometheus']['flags']['storage.tsdb.retention'] = '1d'
end

file 'object-storage-config' do
  path node['thanos']['storage']['config-file']
  content hash_to_yaml(thanos_storage_config)
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0644'
  only_if { node['thanos']['storage']['enable'] }
end

if node['thanos']['tracing']['enable']
  tracing_node_types = %w(thanos-query thanos-sidecar thanos-store thanos-compact thanos-rule)
  tracing_node_types.each do |node_type|
    node.default[node_type]['flags']['tracing.config-file'] = node['thanos']['tracing']['config-file']
  end
end

thanos_tracing_config = {
  type: node['thanos']['tracing']['type'],
  config: node['thanos']['tracing']['config'],
}

file 'tracing-config' do
  path node['thanos']['tracing']['config-file']
  content hash_to_yaml(thanos_tracing_config)
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0644'
  only_if { node['thanos']['tracing']['enable'] }
end

systemd_unit 'thanos-sidecar.service' do
  content Gitlab::Thanos.thanos_unit(node, 'sidecar')
  action [:create, :enable, :start]
  subscribes :restart, 'ark[thanos]', :delayed
  subscribes :restart, 'file[object-storage-config]', :delayed
  subscribes :restart, 'file[tracing-config]', :delayed
  only_if { node['thanos-sidecar']['enable'] }
  notifies :restart, 'systemd_unit[thanos-sidecar.service]', :delayed
end

systemd_unit 'thanos-query.service' do
  content Gitlab::Thanos.thanos_unit(node, 'query')
  action [:create, :enable, :start]
  subscribes :restart, 'ark[thanos]', :delayed
  subscribes :restart, 'file[tracing-config]', :delayed
  only_if { node['thanos-query']['enable'] }
  notifies :restart, 'systemd_unit[thanos-query.service]', :delayed
end

file 'store-selector-config' do
  path node['thanos-store']['flags']['selector.relabel-config-file']
  content hash_to_yaml(node['thanos-store']['selector'])
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0644'
  only_if { node['thanos-store']['enable'] }
end

## Bucket cache config
# https://thanos.io/v0.15/components/store/#caching-bucket
thanos_store_bucket_cache_config = {
  type: 'MEMCACHED',
  config: {
    addresses: [
      "dns+thanos-store-bucket-cache-internal.#{env}.gke.gitlab.net:11211",
    ],
    dns_provider_update_interval: '15s',
    # Limit items to 128MiB to match memcached server setting.
    max_item_size: '128MiB',
    # Default is 10000
    max_async_buffer_size: 100000,
  },
}

file 'store-bucket-cache-config' do
  path node['thanos-store']['bucket-memcached']['config']
  content hash_to_yaml(thanos_store_bucket_cache_config)
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0644'
  only_if { node['thanos-store']['enable'] }
end

node.default['thanos-store']['flags']['store.caching-bucket.config-file'] = node['thanos-store']['bucket-memcached']['config'] if node['thanos-store']['bucket-memcached']['enable']

## Index cache config
# https://thanos.io/v0.15/components/store/#memcached-index-cache
if node['thanos-store']['index-memcached']['enable']
  cache_type = 'MEMCACHED'
  cache_config = {
    addresses: [
      "dns+thanos-store-cache-internal.#{env}.gke.gitlab.net:11211",
    ],
    dns_provider_update_interval: '15s',
    # Limit items to 128MiB to match memcached server setting.
    max_item_size: '128MiB',
    # Default is 10000
    max_async_buffer_size: 100000,
  }
else
  cache_type = 'IN-MEMORY'
  cache_config = {
    max_size: (node['memory']['total'].to_i * 0.10).to_i.to_s + 'KB',
  }
end

thanos_store_index_cache_config = {
  type: cache_type,
  config: cache_config,
}

file 'store-index-cache-config' do
  path node['thanos-store']['flags']['index-cache.config-file']
  content hash_to_yaml(thanos_store_index_cache_config)
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0644'
  only_if { node['thanos-store']['enable'] }
end

systemd_unit 'thanos-store.service' do
  content Gitlab::Thanos.thanos_unit(node, 'store')
  action [:create, :enable, :start]
  subscribes :restart, 'ark[thanos]', :delayed
  subscribes :restart, 'file[object-storage-config]', :delayed
  subscribes :restart, 'file[store-bucket-cache-config]', :delayed
  subscribes :restart, 'file[store-index-cache-config]', :delayed
  subscribes :restart, 'file[store-selector-config]', :delayed
  subscribes :restart, 'file[tracing-config]', :delayed
  only_if { node['thanos-store']['enable'] }
  notifies :restart, 'systemd_unit[thanos-store.service]', :delayed
end

systemd_unit 'thanos-compact.service' do
  content Gitlab::Thanos.thanos_unit(node, 'compact')
  action [:create, :enable, :start]
  subscribes :restart, 'ark[thanos]', :delayed
  subscribes :restart, 'file[object-storage-config]', :delayed
  subscribes :restart, 'file[tracing-config]', :delayed
  only_if { node['thanos-compact']['enable'] }
  notifies :restart, 'systemd_unit[thanos-compact.service]', :delayed
end

# Auto-populate a replica label based on the node name.
if node['thanos-rule']['external_labels']['replica'].nil?
  # Extract the replica number from the hostname.
  replica = node.name[/thanos-[a-z-]*(\d{2,}).*/, 1]
  node.default['thanos-rule']['external_labels']['replica'] = replica unless replica.nil?
end

# Convert the external labels mapping into a list of label flags.
node.default['thanos-rule']['flags']['label'] =
  node['thanos-rule']['external_labels'].map do |label, value|
    "'#{label}=\"#{value}\"'"
  end

include_recipe 'gitlab-prometheus::runbooks' if node['thanos-rule']['enable']

runbooks_dir = File.join(Chef::Config[:file_cache_path], 'runbooks')

systemd_unit 'thanos-rule.service' do
  content Gitlab::Thanos.thanos_unit(node, 'rule')
  action [:create, :enable, :start]
  subscribes :restart, 'ark[thanos]', :delayed
  subscribes :restart, 'file[object-storage-config]', :delayed
  subscribes :restart, 'file[tracing-config]', :delayed
  subscribes :reload, "git[#{runbooks_dir}]", :delayed
  only_if { node['thanos-rule']['enable'] }
  notifies :restart, 'systemd_unit[thanos-rule.service]', :delayed
end

link node['thanos-rule']['rules_dir'] do
  to File.join(runbooks_dir, 'thanos-rules')
  owner node['prometheus']['user']
  group node['prometheus']['group']
  only_if { node['thanos-rule']['enable'] }
end

# Consul registration.

%w(
  compact
  query
  rule
  sidecar
  store
).each do |service|
  next unless node["thanos-#{service}"]['enable']
  next if node['gitlab_consul'].nil?
  next unless node['gitlab_consul']['agent']['enabled'] == true

  port = node['thanos']['http_port']
  consul_definition "thanos-#{service}-http" do
    type 'service'
    parameters(
      check: {
        http: "http://127.0.0.1:#{port}/-/ready",
        interval: '15s',
      },
      name: "thanos-#{service}-http",
      port: port.to_i,
      tags: [
        'metrics',
        service,
      ]
    )
    notifies :reload, 'consul_service[consul]', :delayed
  end
end
