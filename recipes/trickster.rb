include_recipe 'gitlab-prometheus::default'

binary_file  = node['trickster']['binary']
version      = node['trickster']['version']

directory node['trickster']['dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

directory node['trickster']['cache_dir'] do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

ark ::File.basename(node['trickster']['dir']) do
  url node['trickster']['binary_url']
  checksum node['trickster']['checksum']
  version version
  prefix_root Chef::Config['file_cache_path']
  path ::File.dirname(node['trickster']['dir'])
  owner node['prometheus']['user']
  group node['prometheus']['group']
  action :put
  notifies :restart, 'systemd_unit[trickster.service]'
end

link binary_file do
  to File.join(node['trickster']['dir'], "trickster-#{version}", 'bin', "trickster-#{version}.linux-amd64")
  owner node['prometheus']['user']
  group node['prometheus']['group']
end

template node['trickster']['flags']['config'] do
  source 'trickster.conf.erb'
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0644'
  notifies :restart, 'systemd_unit[trickster.service]'
end

trickster_cmd = [binary_file, Gitlab::Prometheus.flags_for(node, 'trickster')].join(' ')

trickster_unit = {
  Unit: {
    Description: 'Trickster',
    Documentation: ['https://github.com/Comcast/trickster'],
    After: 'network.target',
  },
  Service: {
    Type: 'simple',
    ExecStart: trickster_cmd,
    KillMode: 'process',
    MemoryLimit: "#{node['trickster']['memory_kb']}K",
    LimitNOFILE: '1000000',
    Restart: 'always',
    RestartSec: '5s',
    User: node['prometheus']['user'],
  },
  Install: {
    WantedBy: 'multi-user.target',
  },
}

systemd_unit 'trickster.service' do
  content trickster_unit
  action [:create, :enable, :start]
  notifies :restart, 'systemd_unit[trickster.service]', :delayed
end
