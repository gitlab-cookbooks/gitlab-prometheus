include_recipe 'gitlab-prometheus::default'

node.default['ark']['package_dependencies'] = []
include_recipe 'ark::default'

dir_name = ::File.basename(node['pushgateway']['dir'])
dir_path = ::File.dirname(node['pushgateway']['dir'])

ark dir_name do
  url node['pushgateway']['url']
  checksum node['pushgateway']['checksum']
  version node['pushgateway']['version']
  prefix_root Chef::Config['file_cache_path']
  path dir_path
  owner node['prometheus']['user']
  group node['prometheus']['group']
  action :put
  notifies :restart, 'systemd_unit[pushgateway.service]', :delayed
end

pushgateway_cmd = [
  File.join(node['pushgateway']['dir'], 'pushgateway'),
  Gitlab::Prometheus.kingpin_flags_for(node, 'pushgateway'),
].join(' ')

pushgateway_unit = {
  Unit: {
    Description: 'pushgateway',
    Documentation: ['https://github.com/prometheus/pushgateway'],
    After: 'network.target',
  },
  Service: {
    Type: 'simple',
    ExecStart: pushgateway_cmd,
    ExecReload: '/bin/kill -HUP $MAINPID',
    KillMode: 'process',
    LimitNOFILE: '10000',
    Restart: 'always',
    RestartSec: '5s',
    User: node['prometheus']['user'],
  },
  Install: {
    WantedBy: 'multi-user.target',
  },
}

systemd_unit 'pushgateway.service' do
  content pushgateway_unit
  action [:create, :enable, :start]
  notifies :restart, 'systemd_unit[pushgateway.service]', :delayed
end
