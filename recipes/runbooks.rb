# Setup the runbooks git repo.
include_recipe 'gitlab-prometheus::default'

runbook_read_token =  if node['prometheus']['runbook_read_token'].nil?
                        # Vault location: chef/shared/gitlab-prometheus/runbook-read-token
                        get_secrets(node['prometheus']['secrets']['backend'],
                                      node['prometheus']['secrets']['path'],
                                      node['prometheus']['secrets']['key']) unless node['prometheus']['secrets']['backend'].nil?
                      else
                        node['prometheus']['runbook_read_token']
                      end

runbooks_dir = File.join(Chef::Config[:file_cache_path], 'runbooks')

directory runbooks_dir do
  owner node['prometheus']['user']
  group node['prometheus']['group']
  mode '0755'
  recursive true
end

git_repository = if runbook_read_token.nil?
                   node['prometheus']['runbooks']['git_http']
                 else
                   node['prometheus']['runbooks']['git_http'].split('https://').join('https://' + 'oauth2:' + runbook_read_token + '@')
                 end

git runbooks_dir do
  repository git_repository
  revision node['prometheus']['runbooks']['branch']
  user node['prometheus']['user']
  action :sync
end
