control 'pushgateway install' do
  impact 1.0
  title 'Tests Pushgateway Installation'

  describe file('/opt/prometheus/pushgateway/pushgateway') do
    its('mode') { should cmp '0755' }
  end

  describe service('pushgateway') do
    it { should be_enabled }
    it { should be_running }
  end

  describe port(9091) do
    it { should be_listening }
    its('processes') { should include 'pushgateway' }
  end
end
