#
# Cookbook:: GitLab::Monitoring
# Attributes:: prometheus
#
default['prometheus']['version']     = '2.46.0'
default['prometheus']['checksum']    = 'd2177ea21a6f60046f9510c828d4f8969628cfd35686780b3898917ef9c268b9'

default['prometheus']['dir']         = '/opt/prometheus/prometheus'
default['prometheus']['rules_dir'] = "#{node['prometheus']['dir']}/rules"
default['prometheus']['console_templates_dir'] = "#{node['prometheus']['dir']}/consoles"
default['prometheus']['inventory_dir'] = "#{node['prometheus']['dir']}/inventory"
default['prometheus']['binary']      = "#{node['prometheus']['dir']}/prometheus"
default['prometheus']['log_dir']     = '/var/log/prometheus/prometheus'
default['prometheus']['binary_url']  = "https://github.com/prometheus/prometheus/releases/download/v#{node['prometheus']['version']}/prometheus-#{node['prometheus']['version']}.linux-amd64.tar.gz"
default['prometheus']['port']        = 9090

default['prometheus']['scrape_interval'] = '15s'
default['prometheus']['scrape_timeout'] = '10s'
default['prometheus']['evaluation_interval'] = '15s'
default['prometheus']['external_labels'] = {
  'env' => node.chef_environment,
  'environment' => node.chef_environment,
}
default['prometheus']['labels'] = {}

default['prometheus']['alertmanager']['port'] = '9093'
default['prometheus']['alertmanager']['disable'] = false

default['prometheus']['runbooks']['git_http'] =
  if ENV['TEST_KITCHEN'].nil?
    'https://ops.gitlab.net/gitlab-com/runbooks.git'
  else
    # Pull from .com while testing in kitchen - it lacks auth.
    'https://gitlab.com/gitlab-com/runbooks.git'
  end

default['prometheus']['runbooks']['branch']              = 'master'

default['prometheus']['secrets']['backend']       = nil # 'hashicorp_vault'
default['prometheus']['secrets']['path']['path']  = 'shared/gitlab-prometheus'
default['prometheus']['secrets']['path']['mount'] = 'chef'
default['prometheus']['secrets']['key']['key']    = 'runbook-read-token'

default['prometheus']['flags']['config.file']           = "#{node['prometheus']['dir']}/prometheus.yml"
default['prometheus']['flags']['log.format']            = 'json'
default['prometheus']['flags']['web.console.libraries'] = "#{node['prometheus']['dir']}/console_libraries"
default['prometheus']['flags']['web.console.templates'] = "#{node['prometheus']['dir']}/consoles"
default['prometheus']['flags']['web.enable-admin-api']  = true
default['prometheus']['flags']['web.enable-lifecycle']  = true
default['prometheus']['flags']['web.external-url']      = "https://#{node.name}"
default['prometheus']['flags']['web.listen-address']    = ":#{node['prometheus']['port']}"

default['prometheus']['flags']['storage.tsdb.path']      = "#{node['prometheus']['dir']}/data"
default['prometheus']['flags']['storage.tsdb.retention'] = '365d'
default['prometheus']['flags']['storage.tsdb.max-block-duration'] = '7d'
default['prometheus']['flags']['storage.tsdb.wal-compression'] = true

default['prometheus']['flags']['query.max-samples']                = '20000000'
default['prometheus']['flags']['storage.remote.read-sample-limit'] = '20000000'

default['prometheus']['rule_files'] = [
  File.join(node['prometheus']['rules_dir'], '/*.yml'),
]

default['prometheus']['install_method'] = 'binary'

default['prometheus']['jobs'] = []
