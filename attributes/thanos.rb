default['thanos']['version'] = '0.31.0'
default['thanos']['checksum'] = '729da93db0cff899e6f43a4819c84cc794225f76390a76a1bca5dac0c5237a14'
default['thanos']['binary_url'] = "https://github.com/thanos-io/thanos/releases/download/v#{node['thanos']['version']}/thanos-#{node['thanos']['version']}.linux-amd64.tar.gz"

# thanos.additional_{store,query}_targets
# Adds more targets that may not be returned from a chef search
# Format:
# [
#   {
#     "fqdn": "friendly name of the service",
#     "ip": "ip address of the service to target",
#     "port": "port which to target"
#   },
#   ...
# ]
default['thanos']['additional_targets'] = []
default['thanos']['additional_store_targets'] = node['thanos']['additional_targets']
default['thanos']['additional_query_targets'] = []
default['thanos']['dir'] = '/opt/prometheus/thanos'
default['thanos']['binary'] = "#{default['thanos']['dir']}/thanos"
default['thanos']['grpc_port'] = '10901'
default['thanos']['http_port'] = '10902'
default['thanos']['grpc-address'] = "0.0.0.0:#{node['thanos']['grpc_port']}"
default['thanos']['http-address'] = "0.0.0.0:#{node['thanos']['http_port']}"
default['thanos']['store_inventory'] = "#{default['thanos']['dir']}/thanos_store.yml"
default['thanos']['thanos_store_search'] = 'recipes:gitlab-prometheus\\:\\:thanos AND (thanos-rule_enable:true OR thanos-sidecar_enable:true OR thanos-store_enable:true)'
default['thanos']['thanos_public_hosts'] = []
default['thanos']['replica_labels'] = %w(replica prometheus_replica)

default['thanos']['gcs-creds']['backend'] = nil
default['thanos']['gcs-creds']['path'] = 'thanos'
default['thanos']['gcs-creds']['file'] = "#{default['thanos']['dir']}/gcs-creds.json"

default['thanos']['storage']['enable'] = false
default['thanos']['storage']['config-file'] = "#{node['thanos']['dir']}/objstore.yml"
default['thanos']['storage']['type'] = 'GCS'
default['thanos']['storage']['config'] = {}

default['thanos']['tracing']['enable'] = false
default['thanos']['tracing']['config-file'] = "#{node['thanos']['dir']}/tracing.yml"
default['thanos']['tracing']['type'] = 'JAEGER'
default['thanos']['tracing']['config'] = {
  'service_name' => 'thanos',
}

default['thanos-sidecar']['enable'] = false
default['thanos-sidecar']['env'] = {}
default['thanos-sidecar']['log_dir'] = '/var/log/prometheus/thanos-sidecar'
default['thanos-sidecar']['memory_kb'] = 0
default['thanos-sidecar']['flags']['log.format'] = 'json'
default['thanos-sidecar']['flags']['prometheus.url'] = 'http://localhost:9090'
default['thanos-sidecar']['flags']['tsdb.path'] = node['prometheus']['flags']['storage.tsdb.path']
default['thanos-sidecar']['flags']['http-address'] = node['thanos']['http-address']
default['thanos-sidecar']['flags']['grpc-address'] = node['thanos']['grpc-address']
default['thanos-sidecar']['flags']['min-time'] = '-25h'
default['thanos-sidecar']['flags']['shipper.upload-compacted'] = true

default['thanos-query']['enable'] = false
default['thanos-query']['env'] = {}
default['thanos-query']['log_dir'] = '/var/log/prometheus/thanos-query'
default['thanos-query']['memory_kb'] = (node['memory']['total'].to_i * 0.9).to_i
default['thanos-query']['flags']['log.format'] = 'json'
default['thanos-query']['flags']['http-address'] = node['thanos']['http-address']
default['thanos-query']['flags']['query.replica-label'] = node['thanos']['replica_labels']
default['thanos-query']['flags']['query.max-concurrent'] = '20'
default['thanos-query']['flags']['http-address'] = node['thanos']['http-address']
default['thanos-query']['flags']['grpc-address'] = node['thanos']['grpc-address']
default['thanos-query']['flags']['store.sd-files'] = node['thanos']['store_inventory']

default['thanos-store']['enable'] = false
default['thanos-store']['env'] = {}
default['thanos-store']['log_dir'] = '/var/log/prometheus/thanos-store'
default['thanos-store']['index-memcached']['enable'] = true
default['thanos-store']['bucket-memcached']['enable'] = true
default['thanos-store']['bucket-memcached']['config'] = "#{node['thanos']['dir']}/bucket-cache.yml"
default['thanos-store']['memory_kb'] = (node['memory']['total'].to_i * 0.9).to_i
default['thanos-store']['selector'] = [
  {
    "source_labels": [
      'monitor',
    ],
    "regex": '.*',
    "action": 'keep',
  },
]
default['thanos-store']['flags']['log.format'] = 'json'
default['thanos-store']['flags']['data-dir'] = "#{node['thanos']['dir']}/store-data"
default['thanos-store']['flags']['http-address'] = node['thanos']['http-address']
default['thanos-store']['flags']['grpc-address'] = node['thanos']['grpc-address']
default['thanos-store']['flags']['log.level'] = 'debug'
default['thanos-store']['flags']['index-cache.config-file'] = "#{node['thanos']['dir']}/index-cache.yml"
default['thanos-store']['flags']['chunk-pool-size'] = (node['memory']['total'].to_i * 0.10).to_i.to_s + 'KB'
default['thanos-store']['flags']['max-time'] = '-24h'
default['thanos-store']['flags']['selector.relabel-config-file'] = "#{node['thanos']['dir']}/store-selector.yml"
default['thanos-store']['flags']['store.grpc.series-sample-limit'] = 100_000_000.to_s
default['thanos-store']['flags']['store.grpc.series-max-concurrency'] = '50'

default['thanos-compact']['enable'] = false
default['thanos-compact']['env'] = {}
default['thanos-compact']['log_dir'] = '/var/log/prometheus/thanos-compact'
default['thanos-compact']['memory_kb'] = (node['memory']['total'].to_i * 0.9).to_i
default['thanos-compact']['flags']['log.format'] = 'json'
default['thanos-compact']['flags']['data-dir'] = "#{node['thanos']['dir']}/compact-data"
default['thanos-compact']['flags']['http-address'] = node['thanos']['http-address']
default['thanos-compact']['flags']['log.level'] = 'debug'
default['thanos-compact']['flags']['retention.resolution-raw'] = '365d' # 1y
default['thanos-compact']['flags']['retention.resolution-5m'] = '1825d' # 5y
default['thanos-compact']['flags']['retention.resolution-1h'] = '0d' # Inf
default['thanos-compact']['flags']['wait'] = true # Run in daemon mode

default['thanos-rule']['enable'] = false
default['thanos-rule']['env'] = {}
default['thanos-rule']['log_dir'] = '/var/log/prometheus/thanos-rule'
default['thanos-rule']['memory_kb'] = (node['memory']['total'].to_i * 0.9).to_i
default['thanos-rule']['rules_dir'] = "#{node['thanos']['dir']}/thanos-rules"
default['thanos-rule']['external_labels'] = {
  monitor: 'global',
}
# The 'replica' label will always be dropped; add others as necessary
# default['thanos-rule']['drop_alert_labels'] = [ 'foo' ]
default['thanos-rule']['flags']['log.format'] = 'json'
default['thanos-rule']['flags']['data-dir'] = "#{node['thanos']['dir']}/rule-data"
default['thanos-rule']['flags']['grpc-address'] = node['thanos']['grpc-address']
default['thanos-rule']['flags']['http-address'] = node['thanos']['http-address']
default['thanos-rule']['flags']['log.level'] = 'debug'
default['thanos-rule']['flags']['rule-file'] = "#{node['thanos-rule']['rules_dir']}/*.yml"
default['thanos-rule']['flags']['tsdb.retention'] = '7d'
default['thanos-rule']['flags']['query.sd-dns-interval'] = '600s'
default['thanos-rule']['flags']['query'] = 'dns+thanos-query-internal.ops.gke.gitlab.net:9090'
default['thanos-rule']['flags']['alert.label-drop'] = node['thanos']['replica_labels']
default['thanos-rule']['flags']['alertmanagers.sd-dns-interval'] = '600s'
default['thanos-rule']['flags']['alertmanagers.url'] = [
  'dnssrv+http://_alertmanager-internal._tcp.ops.gke.gitlab.net',
]
default['thanos-rule']['flags']['alert.query-url'] = 'https://thanos.gitlab.net'
