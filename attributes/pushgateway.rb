default['pushgateway']['dir'] = '/opt/prometheus/pushgateway'
default['pushgateway']['binary'] = "#{node['pushgateway']['dir']}/pushgateway"
default['pushgateway']['log_dir'] = '/var/log/prometheus/pushgateway'

default['pushgateway']['version'] = '1.3.0'
default['pushgateway']['checksum'] = '1ea7260a41c710a5b0f8ca1b90d18e70afdcc98ce23de61f88aba79a73d85947'
default['pushgateway']['url'] = "https://github.com/prometheus/pushgateway/releases/download/v#{node['pushgateway']['version']}/pushgateway-#{node['pushgateway']['version']}.linux-amd64.tar.gz"

default['pushgateway']['flags']['log.format'] = 'json'
