default['trickster']['version']    = '1.1.3'
default['trickster']['checksum']   = 'ead6bf839acd74c463bbb6a9860072d2bead5e8a182f8551394e0aaca52c189b'

default['trickster']['dir']       = '/opt/prometheus/trickster'
default['trickster']['log_dir']   = '/var/log/prometheus/trickster'
default['trickster']['cache_dir'] = "#{node['trickster']['dir']}/cache"

default['trickster']['binary_url'] = "https://github.com/tricksterproxy/trickster/releases/download/v#{node['trickster']['version']}/trickster-#{node['trickster']['version']}.tar.gz"

default['trickster']['binary']          = "#{node['trickster']['dir']}/trickster"
default['trickster']['flags']['config'] = "#{node['trickster']['dir']}/trickster.conf"

# Allocate 50% of memory to Trickster by default.
default['trickster']['memory_kb'] = (node['memory']['total'].to_i * 0.5).to_i

# Configuration params
default['trickster']['frontend']['listen_port'] = 9095
default['trickster']['frontend']['listen_address'] = '0.0.0.0'
default['trickster']['caches'] = {
  default: {
    cache_type: 'memory',
    options: {
      index: {
        # Allocate 90% of trickster memory ot the default memory cache
        max_size_bytes: (node['trickster']['memory_kb'] * 0.9 * 1024).to_i,
      },
    },
  },
  bbolt: {
    cache_type: 'bbolt',
    options: {
      index: {
        # Default to a 5GiB cache.
        max_size_bytes: 5 * 1024 * 1024 * 1024,
      },
    },
  },
}

default['trickster']['origins'] = {}
default['trickster']['rules'] = {}
default['trickster']['metrics']['listen_port'] = 9195
default['trickster']['metrics']['listen_address'] = '0.0.0.0'
default['trickster']['logging']['log_level'] = 'info'
default['trickster']['logging']['log_file'] = "#{node['trickster']['log_dir']}/trickster.log"
