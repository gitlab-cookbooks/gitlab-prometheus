module Gitlab
  module Thanos
    def self.thanos_unit(node, thanos_mode)
      mode_name = "thanos-#{thanos_mode}"
      binary = node['thanos']['binary']
      flags = Gitlab::Prometheus.kingpin_flags_for(node, mode_name)
      thanos_cmd = [binary, thanos_mode, flags].join(' ')
      thanos_env = node[mode_name]['env'].map do |var, value|
        "\"#{var}=#{value}\""
      end.join(' ')

      {
        Unit: {
          Description: "Thanos #{thanos_mode}",
          Documentation: ['https://thanos.io/'],
          After: 'network.target',
        },
        Service: {
          Type: 'simple',
          Environment: thanos_env,
          ExecReload: '/bin/kill -HUP $MAINPID',
          ExecStart: thanos_cmd,
          KillMode: 'process',
          MemoryLimit: node[mode_name]['memory_kb'] > 0 ? "#{node[mode_name]['memory_kb']}K" : 'infinity',
          LimitNOFILE: '1000000',
          Restart: 'always',
          RestartSec: '5s',
          User: node['prometheus']['user'],
        },
        Install: {
          WantedBy: 'multi-user.target',
        },
      }
    end
  end
end

Chef::DSL::Recipe.include Gitlab::Thanos
Chef::Resource.include Gitlab::Thanos
Chef::DSL::Recipe.include Gitlab::Thanos
