require 'spec_helper'

default_origins = {
  default: {
    origin_type: 'prometheus',
    origin_url: 'http://localhost:9090',
  },
}

rules = {
  example: {
    options: {
      next_route: 'example-route',
      input_source: 'header',
    },
    cases: {
      example_case: {
        matches: %w(
         foo
         bar
        ),
        options: {
          next_route: 'another route',
        },
      },
    },
  },
}

describe 'gitlab-prometheus::trickster' do
  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['trickster']['origins'] = default_origins
        node.normal['trickster']['rules'] = rules
      end.converge(described_recipe)
    end

    it 'creates the systemd service unit' do
      expect(chef_run).to create_systemd_unit('trickster.service')
      expect(chef_run).to enable_systemd_unit('trickster.service')
      expect(chef_run).to start_systemd_unit('trickster.service')
    end

    it 'creates the trickster dir in the configured location' do
      expect(chef_run).to create_directory('/opt/prometheus/trickster').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the trickster cache dir in the configured location' do
      expect(chef_run).to create_directory('/opt/prometheus/trickster/cache').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the configuration file with default content' do
      expect(chef_run).to create_template('/opt/prometheus/trickster/trickster.conf').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0644'
      )
      expect(chef_run).to render_file('/opt/prometheus/trickster/trickster.conf').with_content { |content|
        expect(content).to eq(IO.read('spec/fixtures/trickster.conf'))
      }
    end
  end
end
