require 'spec_helper'

describe 'gitlab-prometheus::pushgateway' do
  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::ServerRunner.new do |node|
      end.converge(described_recipe)
    end

    it 'includes the `ark` recipe' do
      expect(chef_run).to include_recipe('ark::default')
    end

    it 'installs pushgateway' do
      expect(chef_run).to put_ark('pushgateway')
    end

    it 'enables pushgateway service' do
      expect(chef_run).to create_systemd_unit('pushgateway.service').with(
        content: {
          Unit: {
            Description: 'pushgateway',
            Documentation: ['https://github.com/prometheus/pushgateway'],
            After: 'network.target',
          },
          Service: {
            Type: 'simple',
            ExecStart: '/opt/prometheus/pushgateway/pushgateway --log.format=json ',
            ExecReload: '/bin/kill -HUP $MAINPID',
            KillMode: 'process',
            LimitNOFILE: '10000',
            Restart: 'always',
            RestartSec: '5s',
            User: 'prometheus',
          },
          Install: {
            WantedBy: 'multi-user.target',
          },
        }
      )
    end
  end
end
