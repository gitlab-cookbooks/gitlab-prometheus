require 'spec_helper'
require 'prometheus_helper'

describe 'prometheus_helper' do
  before do
    allow_any_instance_of(Chef::Search::Query)
      .to receive(:search).with(:node, 'roles:myrole')
                          .and_return([stub_node('fqdn' => 'my.beautiful.server',
                                                 'hostname' => 'my.hostname')])
    allow_any_instance_of(Chef::Search::Query)
      .to receive(:search).with(:node, 'roles:empty-role')
                          .and_return([])
    allow_any_instance_of(Chef::Search::Query)
      .to receive(:search).with(:node, 'attribute:true')
                          .and_return([stub_node('fqdn' => 'my.beautiful.server.attribute',
                                                 'hostname' => 'my.hostname')])
    allow(Gitlab).to receive(:private_ips_for_node) do |node|
      case node['hostname']
      when 'my.hostname'
        ['192.168.1.2']
      when 'labeled.hostname'
        ['192.168.1.3']
      else
        []
      end
    end
  end

  let(:prometheus_module) do
    c = Class.new { include Gitlab::Prometheus }
    c.new
  end

  it 'can load at least one node' do
    search_query = [{ 'fqdn' => 'my.beautiful.server', 'hostname' => 'my.hostname' }]
    exporter_port = 9100
    public_hosts = []
    additional_targets = []
    inventory = prometheus_module.generate_inventory_file(
      search_query,
      exporter_port,
      public_hosts,
      additional_targets
    )
    expect(inventory).to eq([{ 'targets' => ['192.168.1.2:9100'],
                               'labels' => { 'instance' => 'my.beautiful.server:9100',
                                             'fqdn' => 'my.beautiful.server' } },
    ])
  end

  it 'can load one node with a public ip' do
    search_query = [{ 'fqdn' => 'my.beautiful.server', 'hostname' => 'my.hostname' }]
    exporter_port = 9100
    public_hosts = ['my.beautiful.server']
    additional_targets = []
    inventory = prometheus_module.generate_inventory_file(
      search_query,
      exporter_port,
      public_hosts,
      additional_targets
    )
    expect(inventory).to eq([{ 'targets' => ['my.beautiful.server:9100'],
                               'labels' => { 'fqdn' => 'my.beautiful.server' } },
    ])
  end

  it 'correctly builds additional targets' do
    search_query = [{
      'fqdn' => 'my.beautiful.server',
      'hostname' => 'my.hostname',
    }]
    exporter_port = 9100
    public_hosts = []
    additional_targets = [{
      fqdn: 'gke.server',
      port: '10901',
      ip: '1.1.1.1',
    }]
    inventory = prometheus_module.generate_inventory_file(
      search_query,
      exporter_port,
      public_hosts,
      additional_targets
    )
    expect(inventory).to eq(
      [{
        'targets' => ['192.168.1.2:9100'],
        'labels' => {
          'fqdn' => 'my.beautiful.server',
          'instance' => 'my.beautiful.server:9100',
        },
      }, {
        'targets' => ['1.1.1.1:10901'],
        'labels' => {
          'fqdn' => 'gke.server',
          'instance' => 'gke.server:10901',
        },
      }]
    )
  end

  it 'fails to load a node without an ip' do
    search_query = []
    exporter_port = 9100
    public_hosts = []
    additional_targets = []
    inventory = prometheus_module.generate_inventory_file(
      search_query,
      exporter_port,
      public_hosts,
      additional_targets
    )
    expect(inventory).to be_empty
  end

  it 'searchs for an attribute' do
    search_query = [{ 'fqdn' => 'my.beautiful.server.attribute',
                      'hostname' => 'my.hostname' }]
    exporter_port = 80
    public_hosts = []
    additional_targets = []
    inventory = prometheus_module.generate_inventory_file(
      search_query,
      exporter_port,
      public_hosts,
      additional_targets
    )
    expect(inventory).to eq([{ 'targets' => ['192.168.1.2:80'],
                               'labels' => { 'instance' => 'my.beautiful.server.attribute:80',
                                             'fqdn' => 'my.beautiful.server.attribute' } },
    ])
  end

  it 'raises when attributes are missing' do
    additional_targets = [{
      fqdn: 'gke.server',
      port: '10901',
    }]
    expect { prometheus_module.append_additional_targets(additional_targets) }.to raise_error
  end
end
