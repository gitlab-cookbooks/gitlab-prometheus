require 'spec_helper'
require 'chef-vault'
require 'chef-vault/test_fixtures'

describe 'gitlab-prometheus::prometheus' do
  include ChefVault::TestFixtures.rspec_shared_context

  context 'default execution' do
    before do
      stub_search(:node, 'roles:test-role').and_return(
        [stub_node('fqdn' => 'stubbed_node',
                   'hostname' => 'my.hostname')]
      )
      stub_search(:node, 'recipes:gitlab-prometheus\\:\\:thanos').and_return(
        [
          stub_node('fqdn' => 'fist_node',
                    'hostname' => 'my.hostname',
                    'ipaddress' => '10.0.0.1'),
          stub_node('fqdn' => 'fist_node',
                    'hostname' => 'labeled.hostname',
                    'ipaddress' => '10.0.0.2'),
        ]
      )
      stub_search(:node, 'recipes:gitlab-prometheus\\:\\:thanos AND thanos-query_enable:true').and_return(
        [
          stub_node('fqdn' => 'fist_node',
                    'hostname' => 'my.hostname',
                    'ipaddress' => '10.0.0.1'),
          stub_node('fqdn' => 'fist_node',
                    'hostname' => 'labeled.hostname',
                    'ipaddress' => '10.0.0.2'),
        ]
      )
      stub_search(:node, 'recipes:gitlab-prometheus\\:\\:thanos AND (thanos-rule_enable:true OR thanos-sidecar_enable:true OR thanos-store_enable:true)').and_return(
        [
          stub_node('fqdn' => 'fist_node',
                    'hostname' => 'my.hostname',
                    'ipaddress' => '10.0.0.1'),
          stub_node('fqdn' => 'fist_node',
                    'hostname' => 'labeled.hostname',
                    'ipaddress' => '10.0.0.2'),
        ]
      )
    end

    context 'with a simple chef execution' do
      cached(:chef_run) do
        ChefSpec::ServerRunner.new do |node, server|
          server.create_environment('staging', default_attributes: { cookbook_attr: 'value' })

          # Assign the environment to the node
          node.chef_environment = 'staging'
          node.name 'prometheus-01-test'
          node.normal['prometheus']['jobs']['test-job'] = {
            'static_configs' => ['targets' => ['test-target:8080']],
          }
          node.normal['prometheus']['labels']['shard'] = 'default'
        end.converge('consul::default', described_recipe)
      end

      it 'syncs the runbooks repo' do
        expect(chef_run).to sync_git("#{Chef::Config[:file_cache_path]}/runbooks").with(
          repository: 'https://ops.gitlab.net/gitlab-com/runbooks.git').with(revision: 'master')
      end

      it 'creates the prometheus dir in the configured location' do
        expect(chef_run).to create_directory('/opt/prometheus/prometheus').with(
          owner: 'prometheus',
          group: 'prometheus',
          mode: '0755',
          recursive: true
        )
      end

      it 'deletes the prometheus consoles dir' do
        expect(chef_run).to delete_directory('/opt/prometheus/prometheus/consoles').with(
          recursive: true
        )
      end

      it 'creates the log dir in the configured location' do
        expect(chef_run).to create_directory('/var/log/prometheus/prometheus').with(
          owner: 'prometheus',
          group: 'prometheus',
          mode: '0755',
          recursive: true
        )
      end

      it 'creates the prometheus rules link in the configured location' do
        expect(chef_run).to create_link('/opt/prometheus/prometheus/rules').with(
          owner: 'prometheus',
          group: 'prometheus',
          to: "#{Chef::Config[:file_cache_path]}/runbooks/legacy-prometheus-rules"
        )
      end

      it 'creates the prometheus shard rules link in the configured location' do
        expect(chef_run).to create_link('/opt/prometheus/prometheus/rules-default').with(
          owner: 'prometheus',
          group: 'prometheus',
          to: "#{Chef::Config[:file_cache_path]}/runbooks/legacy-prometheus-rules/default"
        )
      end

      it 'creates the prometheus console templates dir in the configured location' do
        expect(chef_run).to create_link('/opt/prometheus/prometheus/consoles').with(
          owner: 'prometheus',
          group: 'prometheus',
          to: "#{Chef::Config[:file_cache_path]}/runbooks/consoles"
        )
      end

      it 'creates inventory directory' do
        expect(chef_run).to create_directory('/opt/prometheus/prometheus/inventory')
      end

      it 'manages inventory directory' do
        expect(chef_run).to clean_managed_directory('/opt/prometheus/prometheus/inventory')
      end

      it 'installs prometheus' do
        expect(chef_run).to put_ark('prometheus')
      end

      it 'includes runit::default' do
        expect(chef_run).to include_recipe('runit::default')
      end

      it 'runs the prometheus service' do
        expect(chef_run).to enable_runit_service('prometheus').with(
          default_logger: true,
          sv_timeout: 120,
          log_dir: '/var/log/prometheus/prometheus'
        )
      end

      it 'subscribes to git changes' do
        expect(chef_run.runit_service('prometheus')).to subscribe_to("git[#{Chef::Config[:file_cache_path]}/runbooks]").on(:hup)
      end

      it 'creates the configuration file with default content' do
        expect(chef_run).to create_file('/opt/prometheus/prometheus/prometheus.yml').with(
          owner: 'prometheus',
          group: 'prometheus',
          mode: '0644'
        )
        expect(chef_run).to render_file('/opt/prometheus/prometheus/prometheus.yml').with_content { |content|
          expect(content).to eq(IO.read('spec/fixtures/prometheus-default.conf'))
        }
      end
    end

    context 'with relabel configuration' do
      cached(:chef_run_with_relabel) do
        ChefSpec::SoloRunner.new do |node|
          node.name 'prometheus-01-test'
          node.normal['prometheus']['jobs']['test-job'] = {
            'role_name' => ['test-role'],
            'inventory_file_name' => 'test-role',
            'relabel_configs' => 'sentinel',
            'exporter_port' => 9100,
          }
          node.normal['prometheus']['dir'] = '/tmp/prometheus'
        end.converge('consul::default', described_recipe)
      end

      it 'generates a configuration file with jobs and relabel configuration in it' do
        expect(chef_run_with_relabel).to render_file('/tmp/prometheus/inventory/test-role.yml').with_content { |content|
          expect(content).to eq(IO.read('spec/fixtures/inventory-test-role.yml'))
        }
      end
    end

    context 'with relabel configuration' do
      cached(:chef_run_with_relabel) do
        ChefSpec::SoloRunner.new do |node|
          node.name 'prometheus-other-01-test'
          node.normal['prometheus']['scrape_interval'] = '30s'
          node.normal['prometheus']['scrape_timeout'] = '30s'
          node.normal['prometheus']['evaluation_interval'] = '30s'
          node.normal['prometheus']['jobs']['test-job'] = {
            'scrape_interval' => '15s',
            'role_name' => ['test-role'],
            'inventory_file_name' => 'something',
            "file_inventory": true,
            "public_hosts": ['some-public-host'],
            'relabel_configs' => [
              {
                "source_labels": ['__address__'],
                "regex": '(.*)(:80)?',
                "target_label": '__param_target',
                "replacement": '${1}',
              },
              {
                "target_label": '__address__',
                "replacement": 'blackbox.gitlab.com:9115',
              },
            ],
            'exporter_port' => 9100,
          }
        end.converge('consul::default', described_recipe)
      end

      it 'renders the prometheus yaml template' do
        expect(chef_run_with_relabel).to render_file('/opt/prometheus/prometheus/prometheus.yml').with_content { |content|
          expect(content).to eq(IO.read('spec/fixtures/prometheus-with_relabel.conf'))
        }
      end
    end

    context 'with multiple source label rewrite configuration' do
      cached(:chef_run_with_relabel) do
        ChefSpec::SoloRunner.new do |node|
          node.name 'prometheus-01-test'
          node.normal['prometheus']['scrape_interval'] = '30s'
          node.normal['prometheus']['scrape_timeout'] = '30s'
          node.normal['prometheus']['evaluation_interval'] = '30s'
          node.normal['prometheus']['external_labels']['replica'] = 'A'
          node.normal['prometheus']['jobs']['test-job'] = {
            'scrape_interval' => '15s',
            'role_name' => ['test-role'],
            'inventory_file_name' => 'something',
            'exporter_port' => 9100,
            'relabel_configs' => [
              {
                'target_label' => 'baz',
                "source_labels": %w(foo bar),
              },
            ],
          }
        end.converge('consul::default', described_recipe)
      end

      it 'renders the prometheus yaml template' do
        expect(chef_run_with_relabel).to render_file('/opt/prometheus/prometheus/prometheus.yml').with_content { |content|
          expect(content).to eq(IO.read('spec/fixtures/prometheus-with_multi_source_relabel.conf'))
        }
      end
    end
  end

  context 'with a labeled node' do
    before do
      stub_search(:node, 'roles:labeled-node').and_return(
        [stub_node('fqdn' => 'labeled.node',
                   'hostname' => 'labeled.hostname',
                   'prometheus' => {
                     'labels' => {
                       'environment' => 'prod',
                       'tier' => 'fe',
                     },
                   })]
      )
      stub_search(:node, 'recipes:gitlab-alertmanager\\:\\:default').and_return(
        [stub_node('fqdn' => 'stubbed_node',
                   'hostname' => 'my.hostname',
                   'ipaddress' => '10.11.12.13')]
      )
      stub_search(:node, 'recipes:gitlab-prometheus\\:\\:thanos').and_return(
        [
          stub_node('fqdn' => 'fist_node',
                    'hostname' => 'my.hostname',
                    'ipaddress' => '10.0.0.1'),
          stub_node('fqdn' => 'fist_node',
                    'hostname' => 'labeled.hostname',
                    'ipaddress' => '10.0.0.2'),
        ]
      )
      stub_search(:node, 'recipes:gitlab-prometheus\\:\\:thanos AND thanos-query_enable:true').and_return(
        [
          stub_node('fqdn' => 'fist_node',
                    'hostname' => 'my.hostname',
                    'ipaddress' => '10.0.0.1'),
          stub_node('fqdn' => 'fist_node',
                    'hostname' => 'labeled.hostname',
                    'ipaddress' => '10.0.0.2'),
        ]
      )
      stub_search(:node, 'recipes:gitlab-prometheus\\:\\:thanos AND (thanos-rule_enable:true OR thanos-sidecar_enable:true OR thanos-store_enable:true)').and_return(
        [
          stub_node('fqdn' => 'fist_node',
                    'hostname' => 'my.hostname',
                    'ipaddress' => '10.0.0.1'),
          stub_node('fqdn' => 'fist_node',
                    'hostname' => 'labeled.hostname',
                    'ipaddress' => '10.0.0.2'),
        ]
      )
    end
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.name 'prometheus-01-test'
        node.normal['prometheus']['jobs']['test-job'] = {
          'role_name' => ['labeled-node'],
          'inventory_file_name' => 'labeled-node',
          'exporter_port' => 9100,
        }
        node.normal['prometheus']['dir'] = '/tmp/prometheus'
      end.converge('consul::default', described_recipe)
    end

    it 'creates the prometheus dir in the configured location' do
      expect(chef_run).to create_directory('/tmp/prometheus').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates the prometheus rules dir in the configured location' do
      expect(chef_run).to create_link('/tmp/prometheus/rules').with(
        owner: 'prometheus',
        group: 'prometheus',
        to: "#{Chef::Config[:file_cache_path]}/runbooks/legacy-prometheus-rules"
      )
    end

    it 'creates the prometheus console templates dir in the configured location' do
      expect(chef_run).to create_link('/tmp/prometheus/consoles').with(
        owner: 'prometheus',
        group: 'prometheus',
        to: "#{Chef::Config[:file_cache_path]}/runbooks/consoles"
      )
    end

    it 'creates inventory directory' do
      expect(chef_run).to create_directory('/tmp/prometheus/inventory')
    end

    it 'manages inventory directory' do
      expect(chef_run).to clean_managed_directory('/tmp/prometheus/inventory')
    end

    it 'generates an inventory with a node with additional labels' do
      expect(chef_run).to render_file('/tmp/prometheus/inventory/labeled-node.yml').with_content { |content|
        expect(content).to eq(IO.read('spec/fixtures/inventory-test-labels.yml'))
      }
    end
  end
end
