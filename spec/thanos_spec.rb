require 'spec_helper'
require 'chef-vault'
require 'chef-vault/test_fixtures'

object_storage_config = <<-OBJSTORE
---
type: GCS
config:
  bucket: I-Has-A-Bucket
OBJSTORE

tracing_config = <<-TRACING
---
type: JAEGER
config:
  service_name: thanos
TRACING

store_selector_config = <<-STORESELECTOR
---
- source_labels:
  - monitor
  regex: ".*"
  action: keep
STORESELECTOR

describe 'gitlab-prometheus::thanos' do
  include ChefVault::TestFixtures.rspec_shared_context

  context 'default execution' do
    before do
      stub_search(:node, 'recipes:gitlab-prometheus\\:\\:thanos').and_return(
        [
          stub_node('fqdn' => 'fist_node',
                    'hostname' => 'my.hostname',
                    'ipaddress' => '10.0.0.1'),
          stub_node('fqdn' => 'fist_node',
                    'hostname' => 'labeled.hostname',
                    'ipaddress' => '10.0.0.2'),
        ]
      )
      stub_search(:node, 'recipes:gitlab-prometheus\\:\\:thanos AND thanos-query_enable:true').and_return(
        [
          stub_node('fqdn' => 'fist_node',
                    'hostname' => 'my.hostname',
                    'ipaddress' => '10.0.0.1'),
          stub_node('fqdn' => 'fist_node',
                    'hostname' => 'labeled.hostname',
                    'ipaddress' => '10.0.0.2'),
        ]
      )
      stub_search(:node, 'recipes:gitlab-prometheus\\:\\:thanos AND (thanos-rule_enable:true OR thanos-sidecar_enable:true OR thanos-store_enable:true)').and_return(
        [
          stub_node('fqdn' => 'fist_node',
                    'hostname' => 'my.hostname',
                    'ipaddress' => '10.0.0.1'),
          stub_node('fqdn' => 'fist_node',
                    'hostname' => 'labeled.hostname',
                    'ipaddress' => '10.0.0.2'),
        ]
      )
    end

    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.override['thanos-sidecar']['enable'] = true
        node.override['thanos-query']['enable'] = true
        node.override['thanos-store']['enable'] = true
        node.override['thanos-compact']['enable'] = true
        node.override['thanos-rule']['enable'] = true
        node.override['thanos']['gcs-creds']['backend'] = 'chef_vault'
        node.override['thanos']['storage']['enable'] = true
        node.override['thanos']['storage']['config'] = { 'bucket' => 'I-Has-A-Bucket' }
      end.converge('consul::default', described_recipe)
    end

    it 'installs thanos' do
      expect(chef_run).to put_ark('thanos')
    end

    it 'creates the thanos base directory' do
      expect(chef_run).to create_directory('/opt/prometheus/thanos').with(
        owner: 'prometheus',
        group: 'prometheus',
        mode: '0755',
        recursive: true
      )
    end

    it 'creates a bucket config' do
      expect(chef_run).to render_file('/opt/prometheus/thanos/objstore.yml').with_content(object_storage_config)
    end

    it 'creates a gcs creds config' do
      expect(chef_run).to render_file('/opt/prometheus/thanos/gcs-creds.json')
    end

    it 'creates a store selector config' do
      expect(chef_run).to render_file('/opt/prometheus/thanos/store-selector.yml').with_content(store_selector_config)
    end

    it 'creates a store target config' do
      expect(chef_run).to render_file('/opt/prometheus/thanos/thanos_store.yml')
    end

    it 'creates a store bucket cache config' do
      expect(chef_run).to render_file('/opt/prometheus/thanos/bucket-cache.yml').with_content { |content|
        expect(content).to eq(IO.read('spec/fixtures/bucket-cache.yml'))
      }
    end

    it 'creates a store index cache config' do
      expect(chef_run).to render_file('/opt/prometheus/thanos/index-cache.yml').with_content { |content|
        expect(content).to eq(IO.read('spec/fixtures/index-cache-memcached.yml'))
      }
    end

    let(:node) { chef_run.node }

    it 'creates the thanos rules dir in the configured location' do
      expect(chef_run).to create_link('/opt/prometheus/thanos/thanos-rules').with(
        owner: 'prometheus',
        group: 'prometheus',
        to: "#{Chef::Config[:file_cache_path]}/runbooks/thanos-rules"
      )
    end

    it 'creates systemd units' do
      expect(chef_run).to create_systemd_unit('thanos-sidecar.service').with(
        content: {
          Unit: {
            Description: 'Thanos sidecar',
            Documentation: ['https://thanos.io/'],
            After: 'network.target',
          },
          Service: {
            Type: 'simple',
            Environment: '"GOOGLE_APPLICATION_CREDENTIALS=/opt/prometheus/thanos/gcs-creds.json"',
            ExecReload: '/bin/kill -HUP $MAINPID',
            ExecStart: '/opt/prometheus/thanos/thanos sidecar --log.format=json --prometheus.url=http://localhost:9090 --tsdb.path=/opt/prometheus/prometheus/data --http-address=0.0.0.0:10902 --grpc-address=0.0.0.0:10901 --min-time=-25h --shipper.upload-compacted --objstore.config-file=/opt/prometheus/thanos/objstore.yml ',
            KillMode: 'process',
            MemoryLimit: 'infinity',
            LimitNOFILE: '1000000',
            Restart: 'always',
            RestartSec: '5s',
            User: 'prometheus',
          },
          Install: {
            WantedBy: 'multi-user.target',
          },
        }
      )

      expect(chef_run).to create_systemd_unit('thanos-query.service').with(
        content: {
          Unit: {
            Description: 'Thanos query',
            Documentation: ['https://thanos.io/'],
            After: 'network.target',
          },
          Service: {
            Type: 'simple',
            Environment: '',
            ExecReload: '/bin/kill -HUP $MAINPID',
            ExecStart: '/opt/prometheus/thanos/thanos query --log.format=json --http-address=0.0.0.0:10902 --query.replica-label=replica --query.replica-label=prometheus_replica --query.max-concurrent=20 --grpc-address=0.0.0.0:10901 --store.sd-files=/opt/prometheus/thanos/thanos_store.yml ',
            KillMode: 'process',
            MemoryLimit: '943718K',
            LimitNOFILE: '1000000',
            Restart: 'always',
            RestartSec: '5s',
            User: 'prometheus',
          },
          Install: {
            WantedBy: 'multi-user.target',
          },
        }
      )

      expect(chef_run).to create_systemd_unit('thanos-store.service').with(
        content: {
          Unit: {
            Description: 'Thanos store',
            Documentation: ['https://thanos.io/'],
            After: 'network.target',
          },
          Service: {
            Type: 'simple',
            Environment: '"GOOGLE_APPLICATION_CREDENTIALS=/opt/prometheus/thanos/gcs-creds.json"',
            ExecReload: '/bin/kill -HUP $MAINPID',
            ExecStart: '/opt/prometheus/thanos/thanos store --log.format=json --data-dir=/opt/prometheus/thanos/store-data --http-address=0.0.0.0:10902 --grpc-address=0.0.0.0:10901 --log.level=debug --index-cache.config-file=/opt/prometheus/thanos/index-cache.yml --chunk-pool-size=104857KB --max-time=-24h --selector.relabel-config-file=/opt/prometheus/thanos/store-selector.yml --store.grpc.series-sample-limit=100000000 --store.grpc.series-max-concurrency=50 --objstore.config-file=/opt/prometheus/thanos/objstore.yml --store.caching-bucket.config-file=/opt/prometheus/thanos/bucket-cache.yml ',
            KillMode: 'process',
            MemoryLimit: '943718K',
            LimitNOFILE: '1000000',
            Restart: 'always',
            RestartSec: '5s',
            User: 'prometheus',
          },
          Install: {
            WantedBy: 'multi-user.target',
          },
        }
      )

      expect(chef_run).to create_systemd_unit('thanos-compact.service').with(
        content: {
          Unit: {
            Description: 'Thanos compact',
            Documentation: ['https://thanos.io/'],
            After: 'network.target',
          },
          Service: {
            Type: 'simple',
            Environment: '"GOOGLE_APPLICATION_CREDENTIALS=/opt/prometheus/thanos/gcs-creds.json"',
            ExecReload: '/bin/kill -HUP $MAINPID',
            ExecStart: '/opt/prometheus/thanos/thanos compact --log.format=json --data-dir=/opt/prometheus/thanos/compact-data --http-address=0.0.0.0:10902 --log.level=debug --retention.resolution-raw=365d --retention.resolution-5m=1825d --retention.resolution-1h=0d --wait --objstore.config-file=/opt/prometheus/thanos/objstore.yml ',
            KillMode: 'process',
            MemoryLimit: '943718K',
            LimitNOFILE: '1000000',
            Restart: 'always',
            RestartSec: '5s',
            User: 'prometheus',
          },
          Install: {
            WantedBy: 'multi-user.target',
          },
        }
      )

      expect(chef_run).to create_systemd_unit('thanos-rule.service').with(
        content: {
          Unit: {
            Description: 'Thanos rule',
            Documentation: ['https://thanos.io/'],
            After: 'network.target',
          },
          Service: {
            Type: 'simple',
            Environment: '"GOOGLE_APPLICATION_CREDENTIALS=/opt/prometheus/thanos/gcs-creds.json"',
            ExecReload: '/bin/kill -HUP $MAINPID',
            ExecStart: "/opt/prometheus/thanos/thanos rule --log.format=json --data-dir=/opt/prometheus/thanos/rule-data --grpc-address=0.0.0.0:10901 --http-address=0.0.0.0:10902 --log.level=debug --rule-file=/opt/prometheus/thanos/thanos-rules/*.yml --tsdb.retention=7d --query.sd-dns-interval=600s --query=dns+thanos-query-internal.ops.gke.gitlab.net:9090 --alert.label-drop=replica --alert.label-drop=prometheus_replica --alertmanagers.sd-dns-interval=600s --alertmanagers.url=dnssrv+http://_alertmanager-internal._tcp.ops.gke.gitlab.net --alert.query-url=https://thanos.gitlab.net --objstore.config-file=/opt/prometheus/thanos/objstore.yml --label='monitor=\"global\"' ",
            KillMode: 'process',
            MemoryLimit: '943718K',
            LimitNOFILE: '1000000',
            Restart: 'always',
            RestartSec: '5s',
            User: 'prometheus',
          },
          Install: {
            WantedBy: 'multi-user.target',
          },
        }
      )
    end

    it 'thanos-rule subscribes to git changes' do
      expect(chef_run.systemd_unit('thanos-rule.service')).to subscribe_to("git[#{Chef::Config[:file_cache_path]}/runbooks]").on(:reload)
    end

    cached(:chef_run_with_tracing) do
      ChefSpec::SoloRunner.new do |node|
        node.override['thanos-sidecar']['enable'] = true
        node.override['thanos-query']['enable'] = true
        node.override['thanos-store']['enable'] = true
        node.override['thanos-compact']['enable'] = true
        node.override['thanos-rule']['enable'] = true
        node.override['thanos']['gcs-creds']['backend'] = 'chef_vault'
        node.override['thanos']['storage']['enable'] = true
        node.override['thanos']['storage']['config'] = { 'bucket' => 'I-Has-A-Bucket' }

        node.override['thanos-store']['bucket-memcached']['enable'] = false
        node.override['thanos-store']['index-memcached']['enable'] = false

        node.override['thanos']['tracing']['enable'] = true
        node.override['thanos']['tracing']['config'] = { 'service_environment' => 'spec' }
        node.override['thanos']['tracing']['secrets']['backend'] = 'chef_vault'
      end.converge('consul::default', described_recipe)
    end

    it 'creates a store index cache config' do
      expect(chef_run_with_tracing).to render_file('/opt/prometheus/thanos/index-cache.yml').with_content { |content|
        expect(content).to eq(IO.read('spec/fixtures/index-cache-in-memory.yml'))
      }
    end

    it 'creates a tracing config' do
      expect(chef_run_with_tracing).to render_file('/opt/prometheus/thanos/tracing.yml').with_content(tracing_config)
    end

    it 'creates systemd units with tracing' do
      expect(chef_run_with_tracing).to create_systemd_unit('thanos-query.service').with(
        content: {
          Unit: {
            Description: 'Thanos query',
            Documentation: ['https://thanos.io/'],
            After: 'network.target',
          },
          Service: {
            Type: 'simple',
            Environment: '',
            ExecReload: '/bin/kill -HUP $MAINPID',
            ExecStart: '/opt/prometheus/thanos/thanos query --log.format=json --http-address=0.0.0.0:10902 --query.replica-label=replica --query.replica-label=prometheus_replica --query.max-concurrent=20 --grpc-address=0.0.0.0:10901 --store.sd-files=/opt/prometheus/thanos/thanos_store.yml --tracing.config-file=/opt/prometheus/thanos/tracing.yml ',
            KillMode: 'process',
            MemoryLimit: '943718K',
            LimitNOFILE: '1000000',
            Restart: 'always',
            RestartSec: '5s',
            User: 'prometheus',
          },
          Install: {
            WantedBy: 'multi-user.target',
          },
        }
      )

      expect(chef_run_with_tracing).to create_systemd_unit('thanos-store.service').with(
        content: {
          Unit: {
            Description: 'Thanos store',
            Documentation: ['https://thanos.io/'],
            After: 'network.target',
          },
          Service: {
            Type: 'simple',
            Environment: '"GOOGLE_APPLICATION_CREDENTIALS=/opt/prometheus/thanos/gcs-creds.json"',
            ExecReload: '/bin/kill -HUP $MAINPID',
            ExecStart: '/opt/prometheus/thanos/thanos store --log.format=json --data-dir=/opt/prometheus/thanos/store-data --http-address=0.0.0.0:10902 --grpc-address=0.0.0.0:10901 --log.level=debug --index-cache.config-file=/opt/prometheus/thanos/index-cache.yml --chunk-pool-size=104857KB --max-time=-24h --selector.relabel-config-file=/opt/prometheus/thanos/store-selector.yml --store.grpc.series-sample-limit=100000000 --store.grpc.series-max-concurrency=50 --objstore.config-file=/opt/prometheus/thanos/objstore.yml --tracing.config-file=/opt/prometheus/thanos/tracing.yml ',
            KillMode: 'process',
            MemoryLimit: '943718K',
            LimitNOFILE: '1000000',
            Restart: 'always',
            RestartSec: '5s',
            User: 'prometheus',
          },
          Install: {
            WantedBy: 'multi-user.target',
          },
        }
      )
    end
  end
end
